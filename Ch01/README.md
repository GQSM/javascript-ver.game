## Ch01 變數

## 宣告方式
#### 1.`var`
在`JavaScript`更新`ES6`前的宣告方式
```javascript
var name = "神Ｑ"
```
>>>
備註說明：
* `var`可多次宣告同一個變數名稱，也可重複賦值。
* 此宣告方式較不嚴謹，建議視情況使用以下兩種宣告方式。
>>>
#### 2.`let`
是在`ES6`後新增的宣告方式之一
```javascript
let name = "神Ｑ"
```
>>>
備註說明：
* 用`let`宣告的變數名稱無法在同區域內再一次宣告，但可重新賦值。
* 使用上相對於`var`嚴謹，可防止變數名稱重複宣告。
>>>
#### 3.`const`
是在`ES6`後新增的宣告方式之一
```javascript
const name = "神Ｑ"
```
>>>
備註說明：
* 用`const`宣告的變數名稱既無法重複宣告，也無法重新賦值。
* 適合用於固定、防止值被改變的宣告方式。
>>>

## 變數類型
#### 1.`String`(字串)
在`JavaScript`中字串必須在首尾加上雙引號`"`或單引號`'`
```javascript
let strEx1 = "使用頭尾雙引號的字串"
let strEx2 = '使用頭單雙引號的字串'
```
>>>
備註說明：
* 字串只能由成雙的單引號或雙引號包住，若是頭尾符號不符便會出錯，例如：`"ABC'`。
>>>
#### 2.`Int`(數字)
宣告變數時直接輸入數字。
```javascript
let price = 100
ler discount = 0.5 
```
#### 3.`Array`(陣列)
一組陣列需把元素放置`[]`中，並將每個元素用逗點`,`分隔。
```javascript
let arr = ['A',100,['B',200]]
```
>>>
備註說明：
* 陣列中的元素可以是任何型態的變數，包括`string`,`int`或是`Array`和下方介紹的`Object`物件。
* 要將陣列中的值取出來必須在陣列名稱後加上`[]`，並在括號中輸入要取得的索引位置。
    * 例如使用`arr[1]`便能取出陣列`arr`中的第1個索引位置的值。
* 在程式中索引位置由0開始，上方例子陣列`arr`中的`'A'`的索引值就是0。
>>>
#### 4.`Object`(物件)
一個物件必須將把內容放在`{}`之間，物件中的每個內容格式都由一個`key`(鍵)用冒號`:`對應一個`value`(值)，並將每組鍵值由逗號`,`分隔。
```
let obj = {
    name : '神Ｑ',
    age : 18,
    skill : ['HTML','CSS','JavaScript']
} 
```
>>>
備註說明：
* 物件中對應`key`的`value`可以是任一型態的變數，包括`String`、`Int`、`Array`、`Object`。
* 要取得物件內某的`key`(鍵)的`value`(值)，必須在物件後面加上`.`和對應的`key`
    * 例如上方例子，使用`obj.name`便可取得物件`obj`中`name`的值，也就是`'神Ｑ'`，或是使用`obj.skill`取出陣列內容`['HTML','CSS','JavaScript']`。
>>>
## 變數運算
#### 1. 字串相加
```javascript
let strA = 'Hello'
let strB = strA + ' Billy!'

console.log(strB) //'Hello Billy!'
```
>>>
備註說明：
* 字串相加會用組合的方式，由前字串加上後字串。
>>>

### 2. 數值運算
```javascript
let intA = 100
let intB = 10

console.log(intA + intB + 20) //130
console.log(intA - intB) //90
console.log(intA * 2) //200
console.log(intA + intB * 2) //120
```
>>>
備註說明：
* 數值可以使用`+`、`-`、`*`、`/`進行四則運算。
* 程式上也是先乘除後加減。
>>>

## 使用例子
#### 製作角色
1. 宣告一個物件變數
```javascript
let person = {

}
```
2. 在物件內增加姓名`name`、血量`HP`、攻擊力`ATK`、防禦力`DEF`等資料。
```javascript
let person = {
    name : '神Ｑ',
    HP : 1000,
    ATK : 100,
    DEF : 50
}
```
3. 增加一個裝備`equipment`，值為物件，用來儲存武器`arms`、上衣`clothes`、褲子`pants`三個部位的裝備。
```javascript
let person = {
    name : '神Ｑ',
    HP : 1000,
    ATK : 100,
    DEF : 50,
    equipment : {
        arms : '超級太刀',
        clothes : '狗龍重甲',
        pants : '狗龍腿甲'
    }
}
```
#### 製作怪物
1. 宣告一個物件變數
```javascript
let monster = {

}
```
2. 替怪獸增加名字`name`、血量`HP`、攻擊力`ATK`、防禦力`DEF`等資料
```javascript
let monster = {
    name : '史萊姆',
    HP : 200,
    ATK : 30,
    DEF : 10
}
```
3. 增加一個`drop`鍵值，值為陣列，在陣列中用物件紀錄怪物的每一個掉落物資訊，包括掉落品`name`和掉落機率`probability`
```javascript
let monster = {
    name : '史萊姆',
    HP : 200,
    ATK : 30,
    DEF : 10,
    drop : [
        {name : '小刀',probability : 0.3},
        {name : '史萊姆的尾巴',probability : 0.8},
    ]
}
```
經上步驟，便成功建立遊戲中的兩個角色，分別為主角及怪物史萊姆。

完整程式碼：[https://gitlab.com/GQSM/javascript-ver.game/blob/master/Ch01/index.html](https://gitlab.com/GQSM/javascript-ver.game/blob/master/Ch01/index.html)
