##  判斷式和迴圈

---

## 判斷式

---

#### 1.`if`
`if`後面在括號內判斷對`true`或錯`false`，判斷結果在`true`時會執行大括號內的內容
```javascript
let intA = 100
let intB = 1000
if(intA < intB){
    console.log('intA的值比intB還小')
}
```

>>>
備註說明：
* 在`if`括號內的判斷式若不只一個，可用`&&(and)`來區隔每個條件
    * 例如`if(intA > intB && intA <> 0)`，意思是要同時符合`intA`比`intB`還大和`intA`不是0兩種情況。
* 如果擁有兩個條件，但不需同時符合時可以用`||(or)`來區隔每個條件
    * 例如`if(intA > intB || intA > 0)`，意思`intA`比`intB`還大和`intA`大於0其中一種條件符合就執行。
>>>

#### 2. `else if`
當`if`的條件不只一個時可用`else if`在`if`的大括號下增加條件以及符合時執行的內容
```javascript
let intA = 1000
let intB = 100
if(intA < intB){
    console.log('intA的值比intB還小')
}
else if(intB < intA){
    console.log('intB的值比intA還小')
}
```

#### 3. `else`
當`if`或其他`else if`條件都不符合時，可使用`else`來執行判斷外的內容。
```javascript
let intA = 100
let intB = 100
if(intA < intB){
    console.log('intA的值比intB還小')
}
else{
    console.log('intA的值不比intB小')
}
```

>>>
備註說明：
* 判斷式必須由`if`開頭，`else if`及`else`不是必要的。
* 判斷條件一定要放在小括號內，若有多個，可視情況使用`&&`或`||`連接多個條件。
>>>

## 使用例子
#### 比較角色和怪物的攻擊力高低
延上個章節創建的兩個物件`person`及`monster`
1. 從物件內取出角色及怪物的攻擊力和姓名
```javascript
let personATK = person.ATK
let monsterATK = monster.ATK
let personName = person.name 
let monsterName = monster.name 
```
2. 使用`if`比較結果，並用`else`執行不符合條件的另外一個結果
```javascript
if(personATK > monsterATK){
    console.log(personName + '的攻擊力比' + monsterName + '還高')
}
else{
    console.log(personName + '的攻擊力比' + monsterName + '還低')
} 
```

>>>
備註說明：
* 在`console.log`中用了字串組合的方式將判斷結果呈現。
>>>

經上步驟，便成功比較遊戲中的兩個角色的攻擊力高低。

完整程式碼：[https://gitlab.com/GQSM/javascript-ver.game/blob/master/Ch02/ex01/index.html](https://gitlab.com/GQSM/javascript-ver.game/blob/master/Ch02/ex01/index.html)

執行頁面，可點選`F12`開啟`console`查看結果。

[https://gqsm.gitlab.io/javascript-ver.game/Ch02/ex01/index.html](https://gqsm.gitlab.io/javascript-ver.game/Ch02/ex01/index.html)

#### 迴圈

---

#### `for`-基本用法(1)
使用`for`迴圈可在後述括號內註明起始條件、執行條件及每次迴圈後的變動值，每項都由分號`;`分隔，接著大括號內是每次迴圈執行的內容。
```javascript
for(let i=0;i<10;i=i+1){
    console.log(i)
}
```
>>>
備註說明：
* 上述例子中`let i=0`為初始值，`i<10`為執行條件當`i`還不足0時便會一直執行，當大括號內的程式跑完後，會執行`i=i+1`改變初始值，讓每次的`i`在跑完迴圈後都加上1。
* 若是在迴圈的程式內或最後執行處都沒有異動到`i`，那`i`會一直等於0，條件`i<10`會一直成立，形成無窮迴圈，導致程式出錯。
>>>

#### `for`-基本用法(2)
使用`for`搭配`in`可自動讀取物件或陣列中每個索引位置。
```javascript
let skills = ['HTML','CSS','JavaScript']
for(let index in skills){
    console.log(skills[index]) //會依序列出陣列中HTML、CSS、JavaScript三個值
}
```
>>>
備註說明：
* 在`for`的後述括號內建立一個變數`index`，`index`的值會依據`skills`的長度變化。
    * 例如`skills`陣列內有三個索引值，`index`在每次迴圈的值分別是`0`、`1`、`2`。
>>>

#### `while`
在`while`迴圈內只需要設定執行條件。
```javascript
let i = 0
while(i<10){
    console.log(i)
    i=i+1
}
```
>>>
備註說明：
* 和`for`迴圈不同的是，只需要設立條件，設定初始值及異動初始值都要自己在程式內撰寫。
* 若沒有異動初始值`i`，會形成無窮迴圈，導致程式出錯
>>>

## 使用例子
#### 列出怪物的掉落物及機率
使用`for`配合`in`型的迴圈將資訊列出來
1. 將怪物物件`monster`的姓名和掉落物資訊陣列`drop`取出
```javascript
let name = monster.name
let drops = monster.drop
```
2. 使用`for...in`把陣列中的物件依序讀到`drpo`中
```javascript
for(let index in drops){
    console.log(name + '會掉落' + drops[index].name + ',機率是' + drops[index].probability)
}
```

經上步驟，便成功列出該怪物的所有掉落物及機率。

完整程式碼：[https://gitlab.com/GQSM/javascript-ver.game/blob/master/Ch02/ex02/index.html](https://gitlab.com/GQSM/javascript-ver.game/blob/master/Ch02/ex02/index.html)

執行頁面，可點選`F12`開啟`console`查看結果。

[https://gqsm.gitlab.io/javascript-ver.game/Ch02/ex02/index.html](https://gqsm.gitlab.io/javascript-ver.game/Ch02/ex02/index.html)
